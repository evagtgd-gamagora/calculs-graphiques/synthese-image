#pragma once
#include "..\synthese-image-lib\synthese-image-lib.h"

class TestsIntersection
{
public:
	static void testsIntersectionRaySphere();
private:
	static void testIntersectionRaySphere(Ray ray, Sphere sphere);
};

