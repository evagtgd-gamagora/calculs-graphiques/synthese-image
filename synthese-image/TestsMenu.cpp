#include "TestsMenu.h"
#include <iostream>

#include "TestsGestionImages.h"


using namespace std;

void TestsMenu::tests()
{
	//TESTS EN MENU
	cout << "SYNTHESE D'IMAGE : PROGRAMME DE TEST" << endl;

	int choice;
	do {
		cout << "Choisir un test" << endl;
		cout << "0 - Quitter" << endl;
		cout << "1 - Gestion d'images" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			TestsGestionImages::testsSaveImg();
			break;
		default:
			break;
		}
	} while (choice != 0);
}
