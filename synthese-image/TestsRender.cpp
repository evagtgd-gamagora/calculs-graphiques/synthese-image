#include "TestsRender.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace std;
using namespace glm;
using namespace ImageConst;

void TestsRender::testRender()
{
	testFinal();
}

void TestsRender::testFinal()
{
	int C = 800;
	int L = 600;

	vec3 origin = vec3(0, 0, 0);
	vec3 dir = vec3(0, 0, 1);
	float focus = 100;

	Camera camera = Camera(origin, dir, focus, C, L);
	vector<SphericLight> lights = vector<SphericLight>();
	vector<Intersectable*> intersectables;

	//ROOM
	float dist = 2000;
	float R = 20000;

	//LIGHTS
	Light light = Light(vec3(-dist + 300, -dist + 300, -300), vec3(RGBmax, RGBmax * 0.3f, RGBmax), 40000000.0f); //Light from top left
	lights.push_back(SphericLight(light, 10));

	//light = Light(vec3(dist - 300, dist - 300, -300), vec3(RGBmax, RGBmax, RGBmax * 0.3f), 40000000.0f); //Light from bottom right
	//lights.push_back(SphericLight(light, 150));

	//BIG SPHERES
	vector<Sphere> bigSpheres = vector<Sphere>();

	//Walls
	vec3 center = vec3(- R - dist, 0, dist/2);
	Sphere sphere = Sphere(center, R, Material(ImageConst::green, E_BRDF::diffuse));
	bigSpheres.push_back(sphere);

	center = vec3(R + dist, 0, dist/2);
	sphere = Sphere(center, R, Material(ImageConst::blue_light, E_BRDF::diffuse));
	bigSpheres.push_back(sphere);

	center = vec3(0, R + dist, dist/2);
	sphere = Sphere(center, R, Material(ImageConst::white, E_BRDF::diffuse));
	bigSpheres.push_back(sphere);

	center = vec3(0, - R - dist, dist/2);
	sphere = Sphere(center, R, Material(ImageConst::blue, E_BRDF::diffuse));
	bigSpheres.push_back(sphere);

	center = vec3(0, 0, R + dist);
	sphere = Sphere(center, R, Material(ImageConst::white, E_BRDF::diffuse));
	bigSpheres.push_back(sphere);

	center = vec3(0, 0, -R - dist);
	sphere = Sphere(center, R, Material(ImageConst::white, E_BRDF::diffuse));
	bigSpheres.push_back(sphere);

	//Others
	R = 100;
	center = vec3(-250, -250, 300);
	sphere = Sphere(center, R, Material(ImageConst::white,E_BRDF::transparent, IOR_glass));
	bigSpheres.push_back(sphere);

	R = 150;
	center = vec3(0, 0, 450);
	sphere = Sphere(center, R, Material(ImageConst::purple, E_BRDF::specular));
	bigSpheres.push_back(sphere);

	R = 100;
	center = vec3(200, 200, 350);
	sphere = Sphere(center, R, Material(ImageConst::green_light, E_BRDF::transparent, IOR_glass));
	bigSpheres.push_back(sphere);

	R = 300;
	center = vec3(-200, -200, 1000);
	sphere = Sphere(center, R, Material(ImageConst::orange, E_BRDF::diffuse));
	bigSpheres.push_back(sphere);

	R = 400;
	center = vec3(600, -600, 1400);
	sphere = Sphere(center, R, Material(ImageConst::blue_middle, E_BRDF::specular));
	bigSpheres.push_back(sphere);

	for (Sphere& sphere : bigSpheres)
	{
		Sphere * s = &sphere;
		intersectables.push_back((Intersectable*)s);
	}

	//MINI SPHERES
	R = 5;
	vector<Sphere> miniSpheres = vector<Sphere>();
	vector<Sphere *> p_miniSpheres;
	
	//RANDOM
	//for (int x = -10; x <= -7; ++x)
	//{
	//	for (int y = -10; y <= 5; ++y)
	//	{
	//		for (int z = -1; z <= 26; ++z)	
	//		{
	//			center = vec3(- 100 + x * 50 * RandomGenerator::getRandomNormalizedFloat(), - 100 + y * 50 * RandomGenerator::getRandomNormalizedFloat(), 180 + z * 20 * RandomGenerator::getRandomNormalizedFloat());
	//			miniSpheres.push_back(Sphere(center, R, Material(ImageConst::red, E_BRDF::diffuse)));
	//		}
	//	}
	//}

	//for (int x = 0; x <= 10; ++x)
	//{
	//	for (int y = 3 ; y <= 10; ++y)
	//	{
	//		for (int z = -1; z <= 26; ++z)
	//		{
	//			center = vec3(100 + x * 50 * RandomGenerator::getRandomNormalizedFloat(), 100 + y * 50 * RandomGenerator::getRandomNormalizedFloat(), 180 + z * 20 * RandomGenerator::getRandomNormalizedFloat());
	//			miniSpheres.push_back(Sphere(center, R, Material(ImageConst::red, E_BRDF::diffuse)));
	//		}
	//	}
	//}

	//ORGANIZED
	for (int x = 0; x <= 5; ++x)
	{
		for (int y = 0; y <= 5; ++y)
		{
			for (int z = 0; z <= 5; ++z)
			{
				center = vec3(-200 - x * 50, -200 - y * 50, 200 + z * 20);
				miniSpheres.push_back(Sphere(center, R, Material(ImageConst::red, E_BRDF::diffuse)));

				center = vec3(200 + x * 50, 200 + y * 50, 200 + z * 20);
				miniSpheres.push_back(Sphere(center, R, Material(ImageConst::red, E_BRDF::diffuse)));
			}
		}
	}


	for (Sphere& sphere : miniSpheres)
	{
		Sphere * s = &sphere;
		p_miniSpheres.push_back(s);
	}

	BoxStructure bs = BoxStructure(p_miniSpheres);
	
	intersectables.push_back((Intersectable*)&bs);

	//vector<Sphere> boxStructureSpheres;
	//boxStructureSpheres.push_back(Sphere(bs.subA->box.center, length(bs.subA->box.pmax - bs.subA->box.pmin) / 2, Material(ImageConst::red, E_BRDF::diffuse)));
	//boxStructureSpheres.push_back(Sphere(bs.subB->box.center, length(bs.subB->box.pmax - bs.subB->box.pmin) / 2, Material(ImageConst::red, E_BRDF::diffuse)));
	//for (Sphere& sphere : boxStructureSpheres)
	//{
	//	Sphere * s = &sphere;
	//	intersectables.push_back((Intersectable*)s);
	//}


	//RENDERING
	camera.moveFocus(300);
	string fileName = "img/render-sphere-list-light-wide-angle.ppm";
	ImageVector img = camera.render(lights, intersectables, 20, fileName);

	//camera.moveFocus(1000);
	//fileName = "img/render-sphere-list-light-small-angle.ppm";
	//img = camera.render(lights, spheres, 20, fileName);
}