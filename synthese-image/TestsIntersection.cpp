#include "TestsIntersection.h"

#include "..\synthese-image-lib\synthese-image-lib.h"

#include "CImg.h"
#include <math.h>
#include <iostream>

using namespace glm;
using namespace cimg_library;

void TestsIntersection::testsIntersectionRaySphere()
{
	testIntersectionRaySphere(Ray(vec3(1, 1, 0), vec3(2, 2, 0)), Sphere(vec3(5, 5, 0), 0.5));
	testIntersectionRaySphere(Ray(vec3(1, 1, 0), vec3(1, 2, 0)), Sphere(vec3(5, 5, 0), 0.5));
	testIntersectionRaySphere(Ray(vec3(1, 1, 0), vec3(2, 1, 0)), Sphere(vec3(10, 5, 0), 0.5));
	testIntersectionRaySphere(Ray(vec3(1, 1, 0), vec3(2, 1.8, 0)), Sphere(vec3(5, 5, 0), 0.5));
}

void TestsIntersection::testIntersectionRaySphere(Ray ray, Sphere sphere)
{
	CImg<unsigned char> visu(800, 600, 1, 3, 0);

	float factor = std::max(400.0f / abs(sphere.center.x - ray.origin.x), 300.0f / abs(sphere.center.y - ray.origin.y));

	Sphere fSphere = Sphere(sphere.center*factor, sphere.radius*factor);
	const unsigned char red[] = { 255,0,0 }, green[] = { 0,255,0 }, blue[] = { 0,0,255 };

	//Draw points
	visu.draw_circle((int)(ray.origin.x*factor), (int)(ray.origin.y*factor), 2, red);
	visu.draw_circle((int)fSphere.center.x, (int)fSphere.center.y, 2, red);

	//Draw lines
	visu.draw_circle((int)(factor*(ray.origin.x + ray.direction.x)), (int)(factor*(ray.origin.y + ray.direction.y)), 1, blue);
	visu.draw_circle((int)(factor*(ray.origin.x + ray.direction.x / 2)), (int)(factor*(ray.origin.y + ray.direction.y / 2)), 1, blue);
	visu.draw_circle((int)(factor*(ray.origin.x + ray.direction.x / 4)), (int)(factor*(ray.origin.y + ray.direction.y / 4)), 1, blue);
	visu.draw_circle((int)(factor*(ray.origin.x + ray.direction.x / 8)), (int)(factor*(ray.origin.y + ray.direction.y / 8)), 1, blue);

	//Draw spheres
	visu.draw_circle((int)fSphere.center.x, (int)fSphere.center.y, (int)fSphere.radius, green);

	glm::vec3 intersection = vec3(0, 0, 0);
	float distance = 0;

	if (sphere.intersect(ray, &intersection, &distance))
	{
		std::cout << "intersection x : " << intersection.x << "\n";
		std::cout << "intersection y : " << intersection.y << "\n";
		std::cout << "intersection y : " << intersection.z << "\n";
		std::cout << "distance : " << distance << "\n";
		//Draw intersection
		visu.draw_circle((int)(intersection.x*factor), (int)(intersection.y*factor), 2, red);
	}


	CImgDisplay disp(visu, "Intersection test");

	system("pause");
}