#include "TestsGestionImages.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

#include <vector>
#include <math.h>
#include <iostream>
//#include <glm/glm.hpp>
//#include "GestionImgFloat.h"
//#include "ImageVector.h"

#include "..\synthese-image-lib\synthese-image-lib.h"
using namespace std;
using namespace glm;
using namespace ImageConst;


void TestsGestionImages::testsSaveImg() {
	//TESTS
	cout << "[TEST] Debut tests de gestion d'images (creation et enregistrement)" << endl;
	//img blanche
	testImageBlanche();
	//img noir
	testImageNoire();
	//img degradee NB
	testImageDegNB();
	//img degrade couleur
	testImageDegC();
	//img degrade cercle
	testImageCercle();

	cout << "[TEST] Verifier l'aspect des images dans le dossier img" << endl;
}

// TEST d'une image blanche
void TestsGestionImages::testImageBlanche() {
	int C = 5;
	int L = 5;

	string nom = "img/blanc.ppm";


	ImageVector img = ImageVector(C, L, white);
	GestionImg::saveImage(nom, img);
}

//TEST d'une image noire
void TestsGestionImages::testImageNoire() {
	int C = 5;
	int L = 5;

	string nom = "img/noir.ppm";

	ImageVector img = ImageVector(C, L);
	GestionImg::saveImage(nom, img);
}

//TEST d'une image dedgradee NB
void TestsGestionImages::testImageDegNB() {

	//vector<int> pixels;

	int C = 200;
	int L = 200;

	string nom = "img/degNB.ppm";

	ImageVector img = ImageVector(C, L);

	for (int i = 0; i < img.size; ++i)
	{
		float coef = (float)i / (img.size - 1);
		img.setPixelColor(i, white * coef);
	}
	
	GestionImg::saveImage(nom, img);
}



//TEST d'une image dedgradee Couleur
void TestsGestionImages::testImageDegC() {

	int C = 200;
	int L = 200;

	string nom = "img/degC.ppm";

	ImageVector img = ImageVector(C, L);

	for (int i = 0; i < C; ++i)
	{
		for (int j = 0; j < L; ++j)
		{
			float icoef = (float)i / (C - 1);

			float jcoef = (float)j / (L - 1);

			float x = RGBmax * jcoef;
			float y = RGBmax * icoef * jcoef;
			float z = RGBmax * icoef;

			vec3 pixel(x, y, z);

			img.setPixelColor(j*C + i, pixel);
		}
	}
	GestionImg::saveImage(nom, img);
}

//TEST d'une image de cercle
void TestsGestionImages::testImageCercle() {

	int C = 200;
	int L = 200;

	int D = (int) pow(50,2);
	int O = 100;

	vec3 colorCercle = blue_light;
	vec3 colorBkg = purple;

	string nom = "img/cercle.ppm";

	ImageVector img = ImageVector(C, L);

	for (int i = 0; i < C; ++i)
	{
		for (int j = 0; j < L; ++j)
		{
			vec3 pixel = vec3(0, 0, 0);
			if (pow(i-O, 2) + pow(j-O, 2) < D)
				pixel = colorCercle;
			else
				pixel = colorBkg;

			img.setPixelColor(j*C+i, pixel);
		}
	}
	GestionImg::saveImage(nom, img);
}