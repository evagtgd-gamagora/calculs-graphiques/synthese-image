#pragma once
class TestsGestionImages
{
public:
	static void testsSaveImg();
private:
	static void testImageBlanche();
	static void testImageNoire();
	static void testImageDegNB();
	static void testImageDegC();
	static void testImageCercle();
};

