#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;
TEST(TestRadiance, Reflection) {

	vec3 rayOrigin = vec3(1, 1, 1);
	vec3 rayDirection = vec3(-1, -1, -1);
	Ray ray = Ray(rayOrigin, rayDirection);
	vec3 point = vec3(0, 0, 0);
	vec3 normal = vec3(0, 1, 0);

	
	Ray ref = Radiance::reflection(ray, point, normal);
	vec3 expectedDirection = normalize(vec3(-1, 1, -1));

	EXPECT_FLOAT_EQ(ref.direction.x, expectedDirection.x);
	EXPECT_FLOAT_EQ(ref.direction.y, expectedDirection.y);
	EXPECT_FLOAT_EQ(ref.direction.z, expectedDirection.z);

	EXPECT_FLOAT_EQ(ref.origin.x, point.x);
	EXPECT_FLOAT_EQ(ref.origin.y, point.y);
	EXPECT_FLOAT_EQ(ref.origin.z, point.z);
}


TEST(TestRadiance, Schlick) {

	float ior1 = 1.2f;
	float ior2 = 1.7f;

	vec3 i = vec3(1, 0, 0);
	vec3 n = vec3(0, 0, 1);

	float R = Radiance::schlickApproximation(i, n, ior1, ior2);
	EXPECT_GE(R, 0);
	EXPECT_LE(R, 1);
	EXPECT_FLOAT_EQ(R, 1);

	i = vec3(-1, 0, 0);
	R = Radiance::schlickApproximation(i, n, ior1, ior2);
	EXPECT_GE(R, 0);
	EXPECT_LE(R, 1);
	EXPECT_FLOAT_EQ(R, 1);

	i = vec3(0, 0, -1);
	R = Radiance::schlickApproximation(i, n, ior1, ior2);
	EXPECT_GE(R, 0);
	EXPECT_LE(R, 1);
}


TEST(TestRadiance, RandomRebound) {

	bool result;
	float probabilityFactor = 0.f;

	result = Radiance::randomRebound(ImageConst::white, probabilityFactor);
	EXPECT_GE(probabilityFactor, 0);
	EXPECT_LE(probabilityFactor, 1);
	EXPECT_FLOAT_EQ(probabilityFactor, 1);
	EXPECT_TRUE(result);
	

	result = Radiance::randomRebound(ImageConst::black, probabilityFactor);
	EXPECT_GE(probabilityFactor, 0);
	EXPECT_LE(probabilityFactor, 1);
	EXPECT_FALSE(result);

	result = Radiance::randomRebound(ImageConst::red, probabilityFactor);
	EXPECT_GE(probabilityFactor, 0);
	EXPECT_LE(probabilityFactor, 1);

	result = Radiance::randomRebound(ImageConst::red, probabilityFactor);
	EXPECT_GE(probabilityFactor, 0);
	EXPECT_LE(probabilityFactor, 1);

	result = Radiance::randomRebound(ImageConst::red, probabilityFactor);
	EXPECT_GE(probabilityFactor, 0);
	EXPECT_LE(probabilityFactor, 1);

	result = Radiance::randomRebound(vec3(10,20,30), probabilityFactor);
	EXPECT_GE(probabilityFactor, 0);
	EXPECT_LE(probabilityFactor, 1);
	EXPECT_FLOAT_EQ(probabilityFactor, 1);
	EXPECT_TRUE(result);

	result = Radiance::randomRebound(vec3(-10, -20, -30), probabilityFactor);
	EXPECT_GE(probabilityFactor, 0);
	EXPECT_LE(probabilityFactor, 1);
	EXPECT_FALSE(result);
}