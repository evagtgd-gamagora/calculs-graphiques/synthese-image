#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;

TEST(TestSphere, Init) {
	vec3 center = vec3(-90.5f, 940, -564.8f);
	float radius = -512.5f;
	Material mat = Material();

	Sphere sphere1 = Sphere(center, radius, mat);
	Sphere sphere2 = Sphere(center, radius);

	EXPECT_GE(sphere1.radius, 0.0f);
	EXPECT_GE(sphere2.radius, 0.0f);

	EXPECT_FLOAT_EQ(sphere1.radius, sphere2.radius);

	EXPECT_FLOAT_EQ(sphere1.center.x, sphere2.center.x);
	EXPECT_FLOAT_EQ(sphere1.center.y, sphere2.center.y);
	EXPECT_FLOAT_EQ(sphere1.center.z, sphere2.center.z);

	EXPECT_EQ(sphere1.material.reflection, mat.reflection);

	EXPECT_FLOAT_EQ(sphere1.material.color.x, mat.color.x);
	EXPECT_FLOAT_EQ(sphere1.material.color.y, mat.color.y);
	EXPECT_FLOAT_EQ(sphere1.material.color.z, mat.color.z);
}

TEST(TestSphere, Normal)
{
	vec3 center = vec3(1, 1, 1);
	float radius = 10;
	Material mat = Material();

	Sphere sphere1 = Sphere(center, radius, mat);

	vec3 point = vec3(3, 3, 3);
	vec3 normal = sphere1.normal(point);

	vec3 ref = normalize(normal);
	vec3 expected = normalize(point - sphere1.center);

	EXPECT_FLOAT_EQ(ref.x, expected.x);
	EXPECT_FLOAT_EQ(ref.y, expected.y);
	EXPECT_FLOAT_EQ(ref.z, expected.z);
}