#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;

TEST(TestLight, Init) {
	vec3 position = vec3(-90.5f, 940, -564.8f);
	vec3 color = vec3(-10, ImageConst::RGBmax, ImageConst::RGBmax * 10);
	float power = 1000000.f;
	vec3 coloredPower = power * color;

	Light light1 = Light(position, coloredPower);
	Light light2 = Light(position, color, power);

	EXPECT_FLOAT_EQ(light1.position.x, position.x);
	EXPECT_FLOAT_EQ(light1.position.y, position.y);
	EXPECT_FLOAT_EQ(light1.position.z, position.z);

	EXPECT_FLOAT_EQ(light2.position.x, position.x);
	EXPECT_FLOAT_EQ(light2.position.y, position.y);
	EXPECT_FLOAT_EQ(light2.position.z, position.z);

	EXPECT_GE(light1.power.x, 0.0f);
	EXPECT_GE(light1.power.y, 0.0f);
	EXPECT_GE(light1.power.z, 0.0f);

	EXPECT_GE(light2.power.x, 0.0f);
	EXPECT_GE(light2.power.y, 0.0f);
	EXPECT_GE(light2.power.z, 0.0f);
}