#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"
#include <vector>
#include <iostream>

using namespace std;
using namespace glm;

TEST(TestRandomGenerator, randomHemiSphereDirection) {

	vec3 normal;
	vec3 random;

	normal = vec3(1, 1, 1);
	random = RandomGenerator::randomHemisphereDirectionCosWeighted(normal);
	EXPECT_GE(dot(normal, random), 0);

	random = RandomGenerator::randomHemisphereDirectionCosWeighted(normal);
	EXPECT_GE(dot(normal, random), 0);

	random = RandomGenerator::randomHemisphereDirectionCosWeighted(normal);
	EXPECT_GE(dot(normal, random), 0);


	normal = vec3(-20.0f, -90.8f, 369);
	random = RandomGenerator::randomHemisphereDirectionCosWeighted(normal);
	EXPECT_GE(dot(normal, random), 0);

	random = RandomGenerator::randomHemisphereDirectionCosWeighted(normal);
	EXPECT_GE(dot(normal, random), 0);

	random = RandomGenerator::randomHemisphereDirectionCosWeighted(normal);
	EXPECT_GE(dot(normal, random), 0);
}

TEST(TestRandomGenerator, normalizedFloatGeneration) {
	vector<float> values;
	int n = 1000;

	float tolerance = 0.01f;
	float expectedMean = 0.5f;
	float expectedVariance = 1/12.0f;

	float mean = 0;
	float variance = 0;

	for (int i = 0; i < n; ++i)
	{
		float value = RandomGenerator::getRandomNormalizedFloat();
		values.push_back(value);
		mean = (mean * i + value) / (i + 1);

		variance = (variance * i + pow(value,2)) / (i + 1);
	}

	
	variance -= pow(mean, 2);

	EXPECT_GE(mean, expectedMean * (1.0f - tolerance));
	EXPECT_LE(mean, expectedMean * (1.0f + tolerance));

	EXPECT_GE(abs(variance), expectedVariance * (1.0f - tolerance));
}