#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;

TEST(TestMaterial, Init) {
	vec3  albedo = vec3(-500, ImageConst::RGBmax, ImageConst::RGBmax * 10);
	E_BRDF reflection = E_BRDF::specular;

	Material mat1 = Material(albedo, reflection);
	Material mat2 = Material();
	
	EXPECT_GE(mat1.color.x, 0.0f);
	EXPECT_GE(mat1.color.y, 0.0f);
	EXPECT_GE(mat1.color.z, 0.0f);

	EXPECT_LE(mat1.color.x, ImageConst::RGBmax);
	EXPECT_LE(mat1.color.y, ImageConst::RGBmax);
	EXPECT_LE(mat1.color.z, ImageConst::RGBmax);

	EXPECT_GE(mat2.color.x, 0.0f);
	EXPECT_GE(mat2.color.y, 0.0f);
	EXPECT_GE(mat2.color.z, 0.0f);

	EXPECT_LE(mat2.color.x, ImageConst::RGBmax);
	EXPECT_LE(mat2.color.y, ImageConst::RGBmax);
	EXPECT_LE(mat2.color.z, ImageConst::RGBmax);
}