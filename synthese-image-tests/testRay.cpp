#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;

TEST(TestRay, NormalizeDirection) {
	vec3 origin = vec3(0, 0, 0);
	vec3 direction = vec3(20,30,-90);

	Ray ray = Ray(origin, direction);

	EXPECT_EQ(ray.direction, normalize(direction));

	EXPECT_EQ(ray.origin, origin);

	EXPECT_FALSE(false);

	EXPECT_ANY_THROW(Ray(origin, origin));
}