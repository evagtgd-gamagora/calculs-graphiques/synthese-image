#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;

class TestCamera : public::testing::Test{
public:
	// put in any custom data members that you need 
	vec3  origin = vec3(0, 0, 0);
	vec3 direction = vec3(0, 0, 10);
	float focusDistance = -60;
	int nPixelsC = 800;
	int nPixelsL = 600;

	Camera camera = Camera(origin, direction, focusDistance, nPixelsC, nPixelsL);

	TestCamera() {
		// initialization code here
	}

	void SetUp() {
		// code here will execute just before the test ensues 
	}

	void TearDown() {
		// code here will be called just after the test completes
		// ok to through exceptions from here if need be
	}

	~TestCamera() {
		// cleanup any pending stuff, but no exceptions allowed
	}
};

TEST_F(TestCamera, Init) {
	EXPECT_FLOAT_EQ(length(camera.direction), 1.0f);
	EXPECT_FLOAT_EQ(dot(camera.getFocus(), camera.direction), -(length(camera.getFocus()) * length(camera.direction)));
}

TEST_F(TestCamera, MoveFocus) {
	float focusDistance = 90;
	camera.moveFocus(focusDistance);
	EXPECT_FLOAT_EQ(length(camera.getFocus()), focusDistance);
	EXPECT_FLOAT_EQ(dot(camera.getFocus(), camera.direction), -(length(camera.getFocus()) * length(camera.direction)));
}

//TODO