#include "pch.h"

#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;

TEST(TestIntersection, IntersectSphere) {

	vec3 intersection;
	float distance;

	vec3 rayOrigin = vec3(0, 0, 0);
	vec3 rayDir = vec3(1, 1, 1);

	vec3 sphereOrigin = vec3(0, 0, 0);
	float sphereRadius = 100;

	Ray ray = Ray(rayOrigin, rayDir);
	Sphere sphere = Sphere(sphereOrigin, sphereRadius);
	bool ok = Intersection::intersectSphere(ray, sphere, &intersection, &distance);

	EXPECT_EQ(ok, true);
	EXPECT_GE(distance, 0);
	EXPECT_GE(distance, sphereRadius);
	EXPECT_EQ(intersection, normalize(rayDir)*distance);


	sphere.center = vec3(-200, -200, -200);
	ok = Intersection::intersectSphere(ray, sphere, &intersection, &distance);
	EXPECT_EQ(ok, false);

	sphere.center = vec3(200, 200, 200);
	ok = Intersection::intersectSphere(ray, sphere, &intersection, &distance);
	EXPECT_EQ(ok, true);
	EXPECT_GE(distance, 0);
	EXPECT_GE(distance, length(sphere.center) - sphereRadius);
	EXPECT_EQ(intersection, normalize(rayDir)*distance);
}