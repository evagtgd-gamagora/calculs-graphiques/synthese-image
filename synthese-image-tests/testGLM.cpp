#include "pch.h"
#include "..\synthese-image-lib\synthese-image-lib.h"

using namespace glm;

class TestGLM : public ::testing::Test {
public:
	vec3 vNul = vec3(0, 0, 0);
	vec3 a = vec3(-20, 65.780f, -56);
	vec3 b = vec3(30, -68, -25.80f);
};

TEST_F(TestGLM, Length) {
	float resLib = length(vNul);
	float resCalcul = sqrt(pow(vNul.x, 2) + pow(vNul.y, 2) + pow(vNul.z, 2));
	EXPECT_FLOAT_EQ(resLib, resCalcul);

	resLib = length(a);
	resCalcul = sqrt(pow(a.x, 2) + pow(a.y, 2) + pow(a.z, 2));
	EXPECT_FLOAT_EQ(resLib, resCalcul);

	resLib = length(b);
	resCalcul = sqrt(pow(b.x, 2) + pow(b.y, 2) + pow(b.z, 2));
	EXPECT_FLOAT_EQ(resLib, resCalcul);
}

TEST_F(TestGLM, Normalization) {
	vec3 resLib = normalize(a);
	float size = sqrt(pow(a.x, 2) + pow(a.y, 2) + pow(a.z, 2));
	vec3 resCalcul = vec3(a.x / size, a.y / size, a.z / size);
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);

	resLib = normalize(b);
	size = sqrt(pow(b.x, 2) + pow(b.y, 2) + pow(b.z, 2));
	resCalcul = vec3(b.x / size, b.y / size, b.z / size);
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);
}

TEST_F(TestGLM, NormalizationNulBehavior) {
	//No specific behavior
	vec3 resLib = normalize(vNul);

	EXPECT_TRUE(_isnanf(resLib.x));
	EXPECT_TRUE(_isnanf(resLib.y));
	EXPECT_TRUE(_isnanf(resLib.z));
}

TEST_F(TestGLM, ScalarProduct) {
	float resLib = dot(vNul, a);
	float resCalcul = vNul.x * a.x + vNul.y * a.y + vNul.z * a.z;
	EXPECT_FLOAT_EQ(resLib, resCalcul);

	resLib = dot(b, vNul);
	resCalcul = vNul.x * b.x + vNul.y * b.y + vNul.z * b.z;
	EXPECT_FLOAT_EQ(resLib, resCalcul);

	resLib = dot(a, b);
	resCalcul = a.x * b.x + a.y * b.y + a.z * b.z;
	EXPECT_FLOAT_EQ(resLib, resCalcul);
	EXPECT_FLOAT_EQ(resLib, dot(b, a));
}

TEST_F(TestGLM, MultiplyWithConst) {
	float m = (float)(rand()) / (100.0f + 1.0f);
	vec3 resLib = m * vNul;
	vec3 resCalcul = vec3(vNul.x * m, vNul.y * m, vNul.z * m);
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);

	m = (float)(rand()) / (100.0f + 1.0f);
	resLib = m * a;
	resCalcul = vec3(a.x * m, a.y * m, a.z * m);
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);

	m = (float)(rand()) / (100.0f + 1.0f);
	resLib = m * b;
	resCalcul = vec3(b.x * m, b.y * m, b.z * m);
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);
}

TEST_F(TestGLM, Addition) {
	vec3 resLib = vNul + a;
	EXPECT_FLOAT_EQ(resLib.x, a.x);
	EXPECT_FLOAT_EQ(resLib.y, a.y);
	EXPECT_FLOAT_EQ(resLib.z, a.z);

	resLib = a + b;
	vec3 resCalcul = vec3(a.x + b.x, a.y + b.y, a.z + b.z);
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);
}

TEST_F(TestGLM, Subtraction) {
	vec3 resLib = vNul - a;
	EXPECT_FLOAT_EQ(resLib.x, -a.x);
	EXPECT_FLOAT_EQ(resLib.y, -a.y);
	EXPECT_FLOAT_EQ(resLib.z, -a.z);

	resLib = a - b;
	vec3 resCalcul = vec3(a.x - b.x, a.y - b.y, a.z - b.z);
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);
}

TEST_F(TestGLM, VectorialProduct) {
	vec3 resLib = cross(vNul, a);
	EXPECT_FLOAT_EQ(resLib.x, vNul.x);
	EXPECT_FLOAT_EQ(resLib.y, vNul.y);
	EXPECT_FLOAT_EQ(resLib.z, vNul.z);

	resLib = cross(a, b);
	vec3 resCalcul = vec3(a.y*b.z - (a.z*b.y), a.z*b.x - (a.x*b.z), a.x*b.y - (a.y*b.x));
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);

	resLib = cross(b, a);
	resCalcul = vec3(b.y*a.z - (b.z*a.y), b.z*a.x - (b.x*a.z), b.x*a.y - (b.y*a.x));
	EXPECT_FLOAT_EQ(resLib.x, resCalcul.x);
	EXPECT_FLOAT_EQ(resLib.y, resCalcul.y);
	EXPECT_FLOAT_EQ(resLib.z, resCalcul.z);
}