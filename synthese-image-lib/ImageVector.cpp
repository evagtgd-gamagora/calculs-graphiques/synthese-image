#include "pch.h"
#include "ImageVector.h"

using namespace std;
using namespace glm;
using namespace ImageConst;

ImageVector::ImageVector() : C(0), L(0), pixels(NULL)
{
}

ImageVector::ImageVector(int C, int L, vec3 color) : C(C), L(L) 
{
	vector<vec3> pixels;

	for (int i = 0; i < C*L; ++i)
	{
		pixels.push_back(color);
	}
	this->pixels = pixels;
}

ImageVector::ImageVector(int C, int L, std::vector<glm::vec3> pixels) : C(C), L(L), pixels(pixels) 
{
	
}


std::vector<glm::vec3> ImageVector::getPixels() {
	return pixels;
}

void ImageVector::replacePixels(std::vector<glm::vec3> pixels) {
	this->pixels = pixels;
}

glm::vec3 ImageVector::getPixelColor(int c, int l) {
	return getPixelColor(C*l + c);
}
glm::vec3 ImageVector::getPixelColor(int pos) {
	return pixels.at(pos);
}
void ImageVector::setPixelColor(int c, int l, glm::vec3 pixelColor) {
	setPixelColor(C*l + c, pixelColor);
}

void ImageVector::setPixelColor(int pos, glm::vec3 pixelColor) {
	pixels[pos] = pixelColor;
}
