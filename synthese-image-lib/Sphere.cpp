#include "pch.h"
#include "Sphere.h"

using namespace glm;
using namespace ImageConst;

Sphere::Sphere(vec3 center, float radius) : center(center), radius(abs(radius))
{
	this->material = Material(white, E_BRDF::diffuse);
}

Sphere::Sphere(vec3 center, float radius, Material material) : center(center), radius(abs(radius))
{
	this->material = material;
}

vec3 Sphere::normal(vec3 point) const
{
	return normalize(point - this->center);
}

Box Sphere::encompassingBox() const
{
	vec3 increment = vec3(radius, radius, radius);

	return Box(
		center - increment,
		center + increment
	);
}

bool Sphere::intersect(Ray ray, glm::vec3* intersection, float* distance) const
{
	//float a = 1; //Ray is normalized
	float b = 2 * (dot(ray.origin, ray.direction) - dot(this->center, ray.direction));
	vec3 distRS = this->center - ray.origin;
	float c = dot(distRS, distRS) - pow(this->radius, 2);

	float delta = pow(b, 2) - 4 * c;

	if (delta < 0)
		return false;

	float sqrtDelta = sqrt(delta);

	if (-b > sqrtDelta)
		*distance = (-b - sqrtDelta) / 2;
	else
		*distance = (-b + sqrtDelta) / 2;

	//Intersection must be positive to be in the right direction
	if (*distance < 0)
		return false;

	*intersection = ray.origin + *distance * ray.direction;

	return true;
}