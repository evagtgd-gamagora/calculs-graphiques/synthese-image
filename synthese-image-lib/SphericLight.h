#pragma once
#define _USE_MATH_DEFINES
#include "Light.h"

class SphericLight
{
public:
	Light light;
	float radius;

	SphericLight(Light light, float radius);
	glm::vec3 randomHemiSpherePositionFrom(glm::vec3 from) const;
	glm::vec3 getPowerOnPoint() const;
};

