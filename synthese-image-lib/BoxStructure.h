#pragma once
#include "Box.h"
#include "Intersectable.h"
#include "Sphere.h"
#include <vector>

class BoxStructure : public Intersectable
{
public:
	Box box;
	Sphere* sphere;
	//std::vector<Sphere> spheres;
	BoxStructure* subA;
	BoxStructure* subB;

	BoxStructure(std::vector<Sphere*>& spheres);

	bool intersect(Ray ray, glm::vec3* intersection, float* distance) const override;

private:
	enum axis { x, y, z };
	struct axis_sort_result
	{
		axis axis;
		size_t index;
		float cost;
	};

	BoxStructure(std::vector<Sphere*>& spheres, size_t begin, size_t end);
	void init_simple(std::vector<Sphere*>& spheres, size_t begin, size_t end);
	void init_heuristic(std::vector<Sphere*>& spheres, size_t begin, size_t end);
	void init_simple_3d(std::vector<Sphere*>& spheres, size_t begin, size_t end);
	axis_sort_result sortHeuristic(axis axis, std::vector<Sphere*>& spheres, size_t begin, size_t end);
};

