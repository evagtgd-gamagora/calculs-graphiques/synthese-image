#include "pch.h"
#include "Material.h"

using namespace glm;

Material::Material() : color(ImageConst::white), reflection(E_BRDF::diffuse), ior(1.0f)
{
}

Material::Material(vec3 albedo, E_BRDF reflection) : reflection(reflection), ior(1.0f)
{
	albedo.x = clamp(albedo.x, 0.f, ImageConst::RGBmax);
	albedo.y = clamp(albedo.y, 0.f, ImageConst::RGBmax);
	albedo.z = clamp(albedo.z, 0.f, ImageConst::RGBmax);
	this->color = albedo;
}

Material::Material(vec3 albedo, E_BRDF reflection, float ior) : reflection(reflection)
{
	albedo.x = clamp(albedo.x, 0.f, ImageConst::RGBmax);
	albedo.y = clamp(albedo.y, 0.f, ImageConst::RGBmax);
	albedo.z = clamp(albedo.z, 0.f, ImageConst::RGBmax);
	this->color = albedo;
	this->ior = max(0.0f, min(2.5f, ior)); //air 1.0, bubble 1.1, ice 1.310, water 1.333, glass 1.44-1.9, saphire 1.76, diamond 2.5
}