#pragma once
#include "glm/glm.hpp"
#include "Intersectable.h"

class Box : public Intersectable
{
public:
	glm::vec3 pmin, pmax, center;
	Box();
	Box(glm::vec3 pmin, glm::vec3 pmax);
	static Box encompassingBox(Box boxA, Box boxB);

	bool intersect(Ray ray, glm::vec3* intersection, float* distance) const override;
	float surface();
};