#include "pch.h"
#include "Radiance.h"
#include <math.h>
#include "Intersection.h"
#include "Material.h"
#include "RandomGenerator.h"

using namespace std;
using namespace glm;

vec3 Radiance::radianceInit(const Ray& ray, const vector<SphericLight>& lights, const vector<Intersectable*>& intersectables)
{	
	return Radiance::radiance(ray, lights, intersectables, Radiance::recursiveMax);
}

vec3 Radiance::radiance(const Ray& ray, const vector<SphericLight>& lights, const vector<Intersectable*>& intersectables, int depth)
{
	--depth;
	
	if (depth < 0)
		return ImageConst::black;

	vec3 pixel = vec3(0, 0, 0);

	const Sphere *currentSphere;
	vec3 intersection = vec3(0, 0, 0);

	float distance = 0;
	currentSphere = nullptr;

	if (Intersection::intersectScene(true, FLT_MAX, ray, intersectables, &currentSphere, &intersection, &distance))
	{
		switch (currentSphere->material.reflection)
		{
		case E_BRDF::transparent:
			pixel = Radiance::radianceTransparent(ray, lights, intersectables, currentSphere, intersection, depth);
			break; 
		case E_BRDF::specular:
			pixel = Radiance::radianceSpecular(ray, lights, intersectables, currentSphere, intersection, depth);
			break;
		case E_BRDF::diffuse:
			pixel = Radiance::radianceDiffuse(ray, lights, intersectables, currentSphere, intersection, depth);
			break;
		}
	}
	
	return pixel;
}

bool Radiance::randomRebound(const vec3 color, float& probabilityDensityFactor)
{
	vec3 scaledColor = vec3(
		clamp(color.x, 0.0f, ImageConst::RGBmax),
		clamp(color.y, 0.0f, ImageConst::RGBmax),
		clamp(color.z, 0.0f, ImageConst::RGBmax)
	);

	float threshold = std::max(scaledColor.x, std::max(scaledColor.y, scaledColor.z)) / (ImageConst::RGBmax);

	if (RandomGenerator::getRandomNormalizedFloat() >= threshold)
		return false;
	else
	{
		probabilityDensityFactor = threshold;
		return true;
	}
}

vec3 Radiance::radianceDiffuse(const Ray& ray, const vector<SphericLight>& lights, const vector<Intersectable*>& intersectables, const Sphere* currentSphere, const vec3& intersection, int depth)
{
	const Sphere *obstructingSphere;
	vec3 obstruction = vec3(0, 0, 0);

	vec3 pixel = vec3(0, 0, 0);

	//Is this point exposed to light?
	float distance = 0;
	obstructingSphere = NULL;

	//Shift to be sure the intersection point is outside the sphere
	for (auto &light : lights)
	{
		vec3 lightPosition = light.randomHemiSpherePositionFrom(intersection);
		//vec3 lightPosition = light.light.position;
		Ray shiftedRay = Ray(intersection, lightPosition - intersection).shiftedRay();
		float lightDistance = length(lightPosition - intersection);

		//Direct light
		if (!Intersection::intersectScene(false, lightDistance, shiftedRay, intersectables, &obstructingSphere, &obstruction, &distance)) //obtruction sphere must be in front of the light
		{

			vec3 powerFactor = light.getPowerOnPoint();													//Power with coef from random, angle on the light and surface	
			powerFactor /= pow(lightDistance, 2);																	//distance
			powerFactor *= abs(dot(currentSphere->normal(shiftedRay.origin), shiftedRay.direction)) / M_PI;			//angle on the sphere
			

			vec3 colorDirect = currentSphere->material.color * powerFactor;

			pixel += colorDirect;
		}
	}
	
	//TODO
	//Indirect random light
	float randomReboundFactor = 1.f;
	//if (randomRebound(pixel, randomReboundFactor))
	{
		pixel += radianceIndirectDiffuse(ray, lights, intersectables, currentSphere, intersection, depth) / randomReboundFactor; // Correct version but not really smooth
		//pixel /= (1 + randomReboundFactor);		//to keep illuminance stable if few iterations, smooth but not correct
	}

	return pixel;
}

vec3 Radiance::radianceIndirectDiffuse(const Ray& ray, const vector<SphericLight>& lights, const vector<Intersectable*>& intersectables, const Sphere* currentSphere, const vec3& intersection, int depth)
{
	//Random indirect
	//Random direction
	vec3 normal = currentSphere->normal(intersection);
	vec3 indirectDirection = RandomGenerator::randomHemisphereDirectionCosWeighted(normal);
	Ray indirectRay = Ray(intersection, indirectDirection);

	//Integrale hemisphere = 2 * pi
	//Random density Solid Angle = 1 / 2 pi
	//Random density Cos Weighted = cos(theta) / pi
	//BRDF = Albedo * cos(theta) / pi
	//vec3 coef = 2.0f * currentSphere->material.color * dot(normal, indirectRay.direction);
	vec3 coef = currentSphere->material.color;


	return radiance(indirectRay.shiftedRay(), lights, intersectables, depth) * coef;
}


vec3 Radiance::radianceSpecular(const Ray& ray, const vector<SphericLight>& lights, const vector<Intersectable*>& intersectables, const Sphere* currentSphere, const vec3& intersection, int depth)
{
	vec3 normal = currentSphere->normal(intersection);
	Ray reflectRay = reflection(ray, intersection, normal);
	return currentSphere->material.color * radiance(reflectRay.shiftedRay(), lights, intersectables, depth);
}

Ray Radiance::reflection(const Ray& ray, const vec3& point, const vec3& normal)
{
	return Ray(
		point,
		2 * -dot(ray.direction, normal) * normal + ray.direction
	);
}

vec3 Radiance::radianceTransparent(const Ray& ray, const vector<SphericLight>& lights, const vector<Intersectable*>& intersectables, const Sphere* currentSphere, const vec3& intersection, int depth)
{
	vec3 normal = currentSphere->normal(intersection);
	float iorOut;
	float iorIn;

	//Fix direction (in or out sphere)
	if (dot(normal, ray.direction) > 0)
	{
		normal = -normal;
		iorOut = currentSphere->material.ior;
		iorIn = IOR_air;
	}
	else
	{
		iorOut = IOR_air;
		iorIn = currentSphere->material.ior;
	}
		
	float R = schlickApproximation(ray.direction, normal, iorOut, iorIn);
	float random = RandomGenerator::getRandomNormalizedFloat();

	Ray refractRay = ray;
	bool refracted = refraction(ray, intersection, normal, iorOut, iorIn, refractRay);


	//Random Ray - depend on the repartition of quantity of light
	if (!refracted || random < R)
	{
		//reflect
		//coef R & pdf = 1/R
		return radianceSpecular(ray, lights, intersectables, currentSphere, intersection, depth);
	}
	else
	{
		//refract
		//coef (1 - R) & pdf = 1 / (1 - R)			
		return currentSphere->material.color * radiance(refractRay.shiftedRay(), lights, intersectables, depth);
	}
}

bool Radiance::refraction(const Ray& ray, const vec3& point, const vec3& normal, float& iorOut, float& iorIn, Ray& refractRay)
{
	//https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
	//OpenGL calculus to get refracted direction
	float eta = iorOut / iorIn;
	float scalar = dot(ray.direction, normal);
	float k = 1.0f - eta * eta * (1.0f - pow(scalar, 2));
	if (k < 0.0f)
		return false;
	else
	{
		refractRay = Ray(point, eta * ray.direction - (eta * scalar + sqrt(k)) * normal);
		return true;
	}
}

float Radiance::schlickApproximation(const vec3& i, const vec3& n, const float& iorOut, const float& iorIn)
{
	//Schlick approximation - Quantity of light
	float R0 = pow((iorOut - iorIn) / (iorOut + iorIn), 2);
	float R = R0 + (1 - R0) * pow(1 - dot(n, -i), 5);
	return R;
}
 

float Radiance::cosAngleFromVectors(vec3 a, vec3 b)
{
	//Useful if not normalized
	// cos theta = dot(a,b) / (norme(a) * norme(b)) 
	return dot(a, b) / (length(a)*length(b));
}