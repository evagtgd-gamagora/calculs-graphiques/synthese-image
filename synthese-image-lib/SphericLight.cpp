#include "pch.h"
#include "SphericLight.h"
#include "RandomGenerator.h"

using namespace glm;



SphericLight::SphericLight(Light light, float radius) : light(light), radius(abs(radius))
{
}

vec3 SphericLight::randomHemiSpherePositionFrom(vec3 from) const {

	//Determine point on sphere
	vec3 direction = RandomGenerator::randomHemisphereDirectionCosWeighted(from - light.position);
	return light.position + radius * normalize(direction);
}

vec3 SphericLight::getPowerOnPoint() const {
	//Power with coef from random 
	//solid angle (density = 1 / 2 PI R*R) 
	//cos weighted (density = cos(theta) /  PI R*R) 
	//and surface (S = 4 PI R*R)
	//and coef from angle between light and ray cos(theta) / PI
	return light.power / 4.0f;
}