#include "pch.h"

#include <fstream>
#include <iostream>
#include <math.h>

#include "GestionImg.h"
#include "ImageConst.h"

using namespace std;
using namespace glm;

const float GestionImg::RGBscale = 255.0f;

int GestionImg::saveImage(std::string nom, ImageVector img) {
	ofstream myfile;
	myfile.open(nom);
	myfile << "P3\n";
	myfile << img.C << " " << img.L << "\n";
	myfile << GestionImg::RGBscale << "\n";

	vector<vec3> pixels = img.getPixels();
	for (int i = 0; i < img.size; i++)
	{

		vec3 pixel = pixels.at(i);

		pixel = toneMapping(pixel);
		pixel = gammaCorrection(pixel);
		pixel = pixelScale(pixel);

		myfile << pixel.x << " " << pixel.y << " " << pixel.z << "\n";
	}
	myfile.close();

	return 0;
}

vec3 GestionImg::gammaCorrection(vec3 pixel)
{
	pixel.x = pow(pixel.x, 1.0f / 2.2f);
	pixel.y = pow(pixel.y, 1.0f / 2.2f);
	pixel.z = pow(pixel.z, 1.0f / 2.2f);

	return pixel;
}

vec3 GestionImg::pixelScale(vec3 pixel)
{
	//RGB file scale
	pixel = pixel * (GestionImg::RGBscale / ImageConst::RGBmax);

	//Format and clamp
	pixel.x = clamp(trunc(pixel.x), 0.0f, GestionImg::RGBscale);
	pixel.y = clamp(trunc(pixel.y), 0.0f, GestionImg::RGBscale);
	pixel.z = clamp(trunc(pixel.z), 0.0f, GestionImg::RGBscale);

	return pixel;
}

vec3 GestionImg::toneMapping(vec3 pixel)
{
	//log2(1) = 0 ; log2(2) = 1;
	pixel = log2(pixel + vec3(1, 1, 1));
	return pixel;
}