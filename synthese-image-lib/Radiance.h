#pragma once
#define _USE_MATH_DEFINES
#include <vector>
#include <glm/glm.hpp>
#include "Intersectable.h"
#include "SphericLight.h"
#include "Ray.h"
#include "Sphere.h"

class Radiance
{
public:
	static glm::vec3 radianceInit(const Ray& ray, const std::vector<SphericLight>& lights, const std::vector<Intersectable*>& intersectables);
	static bool randomRebound(const glm::vec3 color, float& probabilityDensityFactor);
	static glm::vec3 radiance(const Ray& ray, const std::vector<SphericLight>& lights, const std::vector<Intersectable*>& intersectables, int depth);
	static glm::vec3 radianceDiffuse(const Ray& ray, const std::vector<SphericLight>& lights, const std::vector<Intersectable*>& intersectables, const Sphere* currentSphere, const glm::vec3& intersection, int depth);
	static glm::vec3 radianceIndirectDiffuse(const Ray& ray, const std::vector<SphericLight>& lights, const std::vector<Intersectable*>& intersectables, const Sphere* currentSphere, const glm::vec3& intersection, int depth);
	static glm::vec3 radianceSpecular(const Ray& ray, const std::vector <SphericLight>& lights, const std::vector<Intersectable*>& intersectables, const Sphere* currentSphere, const glm::vec3& intersection, int depth);
	static glm::vec3 radianceTransparent(const Ray& ray, const std::vector <SphericLight>& lights, const std::vector<Intersectable*>& intersectables, const Sphere* currentSphere, const glm::vec3& intersection, int depth);
	static Ray reflection(const Ray& ray, const glm::vec3& point, const glm::vec3& normal);
	static bool refraction(const Ray& ray, const glm::vec3& point, const glm::vec3& normal, float& iorOut, float& iorIn, Ray& refractRay);
	static float schlickApproximation(const glm::vec3& i, const glm::vec3& n, const float& iorOut, const float& iorIn);
	static float cosAngleFromVectors(glm::vec3 a, glm::vec3 b);
private:
	static const int recursiveMax = 5;
};