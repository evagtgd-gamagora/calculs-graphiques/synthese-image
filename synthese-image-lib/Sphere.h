#pragma once
#include <glm/glm.hpp>
#include "Box.h"
#include "ImageConst.h"
#include "Intersectable.h"
#include "Material.h"

class Sphere : public Intersectable
{
public:
	glm::vec3 center;
	float radius;
	Material material;

	Sphere(glm::vec3 center, float radius);
	Sphere(glm::vec3 center, float radius, Material material);
	glm::vec3 normal(glm::vec3 point) const;
	Box encompassingBox() const;

	bool intersect(Ray ray, glm::vec3* intersection, float* distance) const override;
};