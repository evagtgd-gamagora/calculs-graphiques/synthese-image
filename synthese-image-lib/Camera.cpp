#include "pch.h"
#include <math.h>
#include "Camera.h"
#include "GestionImg.h"
#include "Ray.h"
#include "Radiance.h"
#include "RandomGenerator.h"
#include "Intersection.h"


using namespace std;
using namespace glm;

Camera::Camera(vec3  origin, vec3 direction, float focusDistance, int nPixelsC, int nPixelsL) 
	: origin(origin), nPixelsC(nPixelsC), nPixelsL(nPixelsL)
{
	this->direction = normalize(direction);
	this->moveFocus(focusDistance);
}

void Camera::moveFocus(float focusDistance) {
	focus = -abs(focusDistance)*(this->direction);	//Focus is along the same line than direction, except it's behind the sensor
}

glm::vec3 Camera::getFocus()
{
	return focus;
}


ImageVector Camera::render(vector<SphericLight>& lights, vector<Intersectable*>& intersectables, int iteration, string fileName) {
	ImageVector img = ImageVector(nPixelsC, nPixelsL);

	for (int it = 0; it < iteration; ++it) 
	{
		for (int c = 0; c < nPixelsC; ++c)
		{
			for (int l = 0; l < nPixelsL; ++l)
			{
				vec3 rayOrigin = vec3(origin.x + c - nPixelsC / 2, origin.y + l - nPixelsL / 2, origin.z);
				rayOrigin = Camera::antiAliasing(rayOrigin);
				vec3 rayDirection = rayOrigin - focus;
				Ray ray = Ray(rayOrigin, rayDirection);
				
				vec3 color = Radiance::radianceInit(ray, lights, intersectables);

				vec3 pixel = (color + img.getPixelColor(c, l) * (float)it) / (float)(it + 1);
				img.setPixelColor(c, l, pixel);
			}
		}

		
		GestionImg::saveImage(fileName, img);
	}
	
	return img;
}

vec3 Camera::antiAliasing(vec3 origin)
{
	//Random in pixel
	vec3 random = RandomGenerator::randomRectPoint();
	return origin + random;
}