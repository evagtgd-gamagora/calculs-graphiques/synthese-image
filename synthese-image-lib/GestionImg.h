#pragma once
#include <vector>
#include <glm/glm.hpp>
#include "ImageVector.h"

class GestionImg
{
public:
	static const float RGBscale;
	static int saveImage(std::string nom, ImageVector img);
	static glm::vec3 toneMapping(glm::vec3 pixel);
	static glm::vec3 gammaCorrection(glm::vec3 pixel);
	static glm::vec3 pixelScale(glm::vec3 pixel);
};

