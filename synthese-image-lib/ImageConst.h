#pragma once
#include "glm/glm.hpp"

namespace ImageConst
{
	const int Ncanaux = 3;
	const float RGBmax = 1.0f;
	const glm::vec3 white = glm::vec3(RGBmax, RGBmax, RGBmax);
	const glm::vec3 black = glm::vec3(0, 0, 0);
	const glm::vec3 near_black = glm::vec3(0.01f, 0.01f, 0.01f);
	const glm::vec3 red = glm::vec3(RGBmax, 0, 0);
	const glm::vec3 green = glm::vec3(0, RGBmax, 0);
	const glm::vec3 green_light = glm::vec3(RGBmax * 131 / 255.0f, RGBmax, RGBmax * 148 / 255.0f);
	const glm::vec3 blue = glm::vec3(0, 0, RGBmax);
	const glm::vec3 blue_light = glm::vec3(RGBmax * 82 / 255.0f, RGBmax * 187 / 255.0f, RGBmax * 199 / 255.0f);
	const glm::vec3 blue_middle = glm::vec3(RGBmax * 51 / 255.0f, RGBmax * 153 / 255.0f, RGBmax);
	const glm::vec3 purple = glm::vec3(RGBmax * 88 / 255.0f, RGBmax * 82 / 255.0f, RGBmax * 199 / 255.0f);
	const glm::vec3 purple_light = glm::vec3(RGBmax * 192 / 255.0f, RGBmax * 134 / 255.0f, RGBmax);
	const glm::vec3 pink_light = glm::vec3(RGBmax, RGBmax * 204 / 255.0f, RGBmax);
	const glm::vec3 orange = glm::vec3(RGBmax, RGBmax * 83 / 255.0f, RGBmax * 13 / 255.0f);
};