#include "pch.h"
#include "Box.h"
#include <algorithm>

using namespace glm;

Box::Box() {
	Box::Box(vec3(0, 0, 0), vec3(0, 0, 0));
}

Box::Box(vec3 p1, vec3 p2) {
	pmin = vec3(
		std::min(p1.x, p2.x),
		std::min(p1.y, p2.y),
		std::min(p1.z, p2.z)

	);
	pmax = vec3(
		std::max(p1.x, p2.x),
		std::max(p1.y, p2.y),
		std::max(p1.z, p2.z)

	);
	center = vec3(
		(pmax.x - pmin.x)/2 + pmin.x,
		(pmax.y - pmin.y)/2 + pmin.y,
		(pmax.z - pmin.z)/2 + pmin.z
	);
}

Box Box::encompassingBox(Box boxA, Box boxB) {
	Box encompassingBox = Box(
		vec3(
			std::min(boxA.pmin.x, boxB.pmin.x), 
			std::min(boxA.pmin.y, boxB.pmin.y), 
			std::min(boxA.pmin.z, boxB.pmin.z) 
			),
		vec3(
			std::max(boxA.pmax.x, boxB.pmax.x),
			std::max(boxA.pmax.y, boxB.pmax.y),
			std::max(boxA.pmax.z, boxB.pmax.z)
			)
	);

	return encompassingBox;
}

bool Box::intersect(Ray ray, vec3* intersection, float* distance) const
{
	vec3 inverseRay = vec3(1.0f / ray.direction.x, 1.0f / ray.direction.y, 1.0f / ray.direction.z);
	float tmin;
	float tmax;

	//x slab
	float tx1 = (this->pmin.x - ray.origin.x) * inverseRay.x;
	float tx2 = (this->pmax.x - ray.origin.x) * inverseRay.x;
	
	if (tx1 <= 0 && tx2 <= 0)
		return false;

	tmin = std::min(tx1, tx2);
	tmax = std::max(tx1, tx2);

	//y slab
	float ty1 = (this->pmin.y - ray.origin.y) * inverseRay.y;
	float ty2 = (this->pmax.y - ray.origin.y) * inverseRay.y;

	if (ty1 <= 0 && ty2 <= 0)
		return false;

	tmin = std::max(tmin, std::min(ty1, ty2));
	tmax = std::min(tmax, std::max(ty1, ty2));

	//z slab
	float tz1 = (this->pmin.z - ray.origin.z) * inverseRay.z;
	float tz2 = (this->pmax.z - ray.origin.z) * inverseRay.z;

	if (tz1 <= 0 && tz2 <= 0)
		return false;

	tmin = std::max(tmin, std::min(tz1, tz2));
	tmax = std::min(tmax, std::max(tz1, tz2));

	if (tmax >= tmin)
	{
		if (tmin >= 0)
		{
			*distance = tmin;
			*intersection = ray.origin + ray.direction * tmin;
			return true;
		}
		else if (tmax >= 0)
		{
			*distance = tmax;
			*intersection = ray.origin + ray.direction * tmax;
			return true;
		}
	}

	return false;
}

float Box::surface()
{
	float surface = 0;

	surface += (pmax.x - pmin.x)*(pmax.y - pmin.y);
	surface += (pmax.x - pmin.x)*(pmax.z - pmin.z);
	surface += (pmax.z - pmin.z)*(pmax.y - pmin.y);

	surface *= 2;
	return surface;
}