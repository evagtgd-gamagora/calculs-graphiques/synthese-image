#include "pch.h"
#include "Light.h"
#include "ImageConst.h"

using namespace glm;

Light::Light(vec3 position, vec3 power) : position(position)
{
	this->power = vec3(0, 0, 0);
	this->power.x = max(0.f, power.x);
	this->power.x = max(0.f, power.y);
	this->power.x = max(0.f, power.z);
}

Light::Light(vec3 position, vec3 color, float power) : position(position)
{
	power = max(0.f, power);
	
	color.x = clamp(color.x, 0.f, ImageConst::RGBmax);
	color.y = clamp(color.y, 0.f, ImageConst::RGBmax);
	color.z = clamp(color.z, 0.f, ImageConst::RGBmax);

	this->power = power * color;
}