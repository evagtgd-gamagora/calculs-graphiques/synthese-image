#pragma once
#include <vector>
#include <glm/glm.hpp>
#include "Box.h"
#include "Intersectable.h"
#include "Triangle.h"
#include "Ray.h"
#include "Sphere.h"


class Intersection
{
	public:
		static bool intersectScene(bool nearest, const float dMax, const Ray ray, const std::vector<Intersectable*>& intersectables, const Sphere** intersectingSphere, glm::vec3* intersection, float* distance);
		//static bool intersectScene(bool nearest, const float dMax, const Ray ray, const std::vector<Intersectable>& intersectable, const Intersectable** intersected, glm::vec3* intersection, float* distance);
};