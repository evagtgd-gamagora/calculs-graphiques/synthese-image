#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "ImageConst.h"

class ImageVector
{
public:
	ImageVector();
	ImageVector(int C, int L, glm::vec3 color = ImageConst::black);		//couleur par defaut Noire
	ImageVector(int C, int L, std::vector<glm::vec3> pixels);
	
	int C;
	int L;
	int size = C * L;

	std::vector<glm::vec3> getPixels();
	void replacePixels(std::vector<glm::vec3> pixels);
	glm::vec3 getPixelColor(int c, int l);
	glm::vec3 getPixelColor(int pos);
	void setPixelColor(int c, int l, glm::vec3 pixelColor);
	void setPixelColor(int pos, glm::vec3 pixelColor);

private :
	std::vector<glm::vec3> pixels;
};


