#pragma once
#include "glm/glm.hpp"
#include "Intersectable.h"

class Triangle
{
public:
	glm::vec3 p0, p1, p2;
	Triangle(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2);
};

