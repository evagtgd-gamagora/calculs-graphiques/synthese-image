#include "pch.h"
#include "BoxStructure.h"
#include <algorithm>
#include <math.h>
#include <stdexcept>
#include <glm/glm.hpp>

using namespace std;

bool BoxStructure::intersect(Ray ray, glm::vec3* intersection, float* distance) const
{
	return this->box.intersect(ray, intersection, distance);
}

BoxStructure::BoxStructure(vector<Sphere*>& spheres) {
	if (spheres.empty())
	{
		throw invalid_argument("BoxStructure : there are no spheres in constructor param");
	}
	else
	{

		//init_simple(spheres, 0, spheres.size() - 1);

		init_simple_3d(spheres, 0, spheres.size() - 1);
			
		//init_heuristic(spheres, 0, spheres.size() - 1);
	}
}

BoxStructure::BoxStructure(vector<Sphere*>& spheres, size_t begin, size_t end) {
	if (begin > end)
	{
		throw invalid_argument("BoxStructure : there are no spheres in constructor param");
	}
	else 
	{
		//init_simple(spheres, begin, end);

		init_simple_3d(spheres, begin, end);

		//init_heuristic(spheres, begin, end);
	}
}


//Split on x axis, median
void BoxStructure::init_simple(vector<Sphere*>& spheres, size_t begin, size_t end)
{
	size_t dist = end - begin + 1;

	if (dist <= 1)
	{
		this->sphere = spheres[begin];
		this->box = this->sphere->encompassingBox();
		this->subA = nullptr;
		this->subB = nullptr;
	}

	else
	{
		this->sphere = nullptr;
		dist /= 2;

		sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.x < b->center.x; });

		this->subA = new BoxStructure(spheres, begin, begin + dist - 1);
		this->subB = new BoxStructure(spheres, begin + dist, end);
		this->box = Box::encompassingBox(subA->box, subB->box);
	}
}

//Split on wider axis, median
void BoxStructure::init_simple_3d(vector<Sphere*>& spheres, size_t begin, size_t end)
{
	size_t dist = end - begin + 1;

	if (dist <= 1)
	{
		this->sphere = spheres[begin];
		this->box = this->sphere->encompassingBox();
		this->subA = nullptr;
		this->subB = nullptr;
	}

	else
	{
		this->sphere = nullptr;
		dist /= 2;
		
		this->box = spheres[begin]->encompassingBox();

		for (size_t i = begin + 1; i <= end; ++i)
		{
			this->box = Box::encompassingBox(this->box, spheres[i]->encompassingBox());
		}

		float x = this->box.pmax.x - this->box.pmin.x;
		float y = this->box.pmax.y - this->box.pmin.y;
		float z = this->box.pmax.z - this->box.pmin.z;

		if(x > y && x > z)
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.x < b->center.x; });
		else if (y > z)
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.y < b->center.y; });
		else
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.z < b->center.z; });

		this->subA = new BoxStructure(spheres, begin, begin + dist - 1);
		this->subB = new BoxStructure(spheres, begin + dist, end);
	}
}


//Split on 3 axis, depending on box surface
void BoxStructure::init_heuristic(vector<Sphere*>& spheres, size_t begin, size_t end)
{
	size_t dist = end - begin + 1;

	if (dist <= 1)
	{
		this->sphere = spheres[begin];
		this->box = this->sphere->encompassingBox();
		this->subA = nullptr;
		this->subB = nullptr;
	}

	else
	{
		this->sphere = nullptr;

		axis_sort_result result_x = sortHeuristic(axis::x, spheres, begin, end);
		axis_sort_result result_y = sortHeuristic(axis::y, spheres, begin, end);
		axis_sort_result result_z = sortHeuristic(axis::z, spheres, begin, end);
		size_t index = begin;

		if (result_x.cost < result_y.cost && result_x.cost < result_z.cost)
		{
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.x < b->center.x; });
			index = result_x.index;
		}
			
		else if (result_y.cost < result_z.cost)
		{
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.y < b->center.y; });
			index = result_y.index;
		}
		else
		{
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.z < b->center.z; });
			index = result_z.index;
		}

		this->subA = new BoxStructure(spheres, begin, begin + index);
		this->subB = new BoxStructure(spheres, begin + index + 1, end);
		this->box = Box::encompassingBox(subA->box, subB->box);
	}
}

BoxStructure::axis_sort_result BoxStructure::sortHeuristic(axis axis, vector<Sphere*>& spheres, size_t begin, size_t end)
{
	axis_sort_result result;
	result.axis = axis;
	result.cost = FLT_MAX;
	result.index = 0;

	switch (axis)
	{
		case axis::x :
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.x < b->center.x; });
			break;
		case axis::y :
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.y < b->center.y; });
			break;
		case axis::z :
			sort(spheres.begin() + begin, spheres.begin() + end + 1, [](Sphere * a, Sphere *b) { return a->center.z < b->center.z; });
			break;

	}
	

	vector<Box> boxesA;
	vector<Box> boxesB;

	boxesA.push_back(spheres[begin]->encompassingBox());
	boxesB.push_back(spheres[end]->encompassingBox());


	for (int i = 1; i <= end - begin; ++i)
	{
		boxesA.push_back(Box::encompassingBox(boxesA[i - 1], spheres[begin + i]->encompassingBox()));
		boxesB.push_back(Box::encompassingBox(boxesB[i - 1], spheres[end - i]->encompassingBox()));
	}

	for (int i = 0; i <= end - begin; ++i)
	{
		size_t na = i + 1;
		size_t nb = end - begin - i;
		float temp_cost = boxesA[i].surface() * na + boxesB[end - begin - i].surface() * nb;

		if (result.cost > temp_cost)
		{
			result.cost = temp_cost;
			result.index = i;
		}
	}

	return result;
}