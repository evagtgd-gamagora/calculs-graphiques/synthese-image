#pragma once
#include <glm/glm.hpp>
#include "ImageConst.h"

enum E_BRDF { diffuse, specular, transparent};

class Material
{
public:
	E_BRDF reflection;
	glm::vec3 color;
	float ior;
	

	Material();
	Material(glm::vec3 albedo, E_BRDF reflection);
	Material(glm::vec3 albedo, E_BRDF reflection, float ior);
};

const float IOR_air = 1.0f;
const float IOR_glass = 1.52f;
const Material mirror = Material(ImageConst::white, E_BRDF::specular);
const Material glass = Material(ImageConst::white, E_BRDF::transparent, IOR_glass);
