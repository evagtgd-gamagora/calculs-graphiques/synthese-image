#pragma once
#define _USE_MATH_DEFINES
#include <vector>
#include <glm/glm.hpp>
#include "ImageVector.h"
#include "Intersectable.h"
#include "Sphere.h"
#include "SphericLight.h"

class Camera
{
public:
	glm::vec3  origin;		//Sensor origin
	glm::vec3 direction;	//Sensor direction
	int nPixelsC;			//Sensor size
	int nPixelsL;			//Sensor size

	Camera(glm::vec3  origin, glm::vec3 direction, float focusDistance, int nPixelsC, int nPixelsL);
	void moveFocus(float focusDistance);
	glm::vec3 getFocus();
	ImageVector render(std::vector<SphericLight>& light, std::vector<Intersectable*>& intersectable, int iteration, std::string fileName);
	glm::vec3 antiAliasing(glm::vec3 origin);

private:
	glm::vec3 focus;		//Focus point
};

