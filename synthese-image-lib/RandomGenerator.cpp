#include "pch.h"
#include "RandomGenerator.h"

using namespace std;
using namespace glm;

uniform_real_distribution<float> RandomGenerator::uniformRand = uniform_real_distribution<float>(0.0f, 1.0f);
mt19937 RandomGenerator::randomEngine = mt19937(0);

float RandomGenerator::getRandomNormalizedFloat()
{
	return RandomGenerator::uniformRand(RandomGenerator::randomEngine);
}

vec3 RandomGenerator::randomUnitHemisphereDirectionSolidAngle()
{
	//Generate random direction on unit hemisphere proportional to solid angle
	//Normal on z-axis

	//Random value
	float r1 = RandomGenerator::getRandomNormalizedFloat();
	float r2 = RandomGenerator::getRandomNormalizedFloat();

	float phi = 2.0f * (float)M_PI * r1;
	float coef = sqrt(1 - pow(r2, 2));

	//Determine value when normal is on z axis
	vec3 direction = vec3();
	direction.x = cos(phi) * coef;
	direction.y = sin(phi) * coef;
	direction.z = r2;

	return direction;
}

vec3 RandomGenerator::randomUnitHemisphereDirectionCosWeighted()
{
	//Generate random direction on unit hemisphere proportional to cosine-weighted solid angle
	//Normal on z-axis

	//Random value
	float r1 = RandomGenerator::getRandomNormalizedFloat();
	float r2 = RandomGenerator::getRandomNormalizedFloat();

	float phi = 2.0f * (float)M_PI * r1;
	float coef = sqrt(1 - r2);

	//Determine value when normal is on z axis
	vec3 direction = vec3();
	direction.x = cos(phi) * coef;
	direction.y = sin(phi) * coef;
	direction.z = sqrt(r2);

	return direction;
}

RandomGenerator::basis RandomGenerator::changeVector3DBasis(vec3 normal)
{
	normal = normalize(normal);

	//Determine new referential
	vec3 anyVector1 = normalize(vec3(1, 1, 1));
	vec3 anyVector2 = normalize(vec3(-1, -1, 1));

	vec3 x1 = normalize(cross(anyVector1, normal));
	vec3 x2 = normalize(cross(anyVector2, normal));
	vec3 x = vec3();

	float collinear = abs(dot(x1, normal));
	if (collinear > 0.9)
		x = x2;
	else
		x = x1;

	vec3 y = normalize(cross( normal, x));

	basis newBasis;
	newBasis.x = x;
	newBasis.y = y;
	newBasis.z = normal;

	return newBasis;
}

vec3 RandomGenerator::randomHemisphereDirectionSolidAngle(vec3 normal)
{
	//Determine random direction when normal is on z axis
	vec3 direction = RandomGenerator::randomUnitHemisphereDirectionSolidAngle();

	//Change Vector Basis
	basis newBasis = changeVector3DBasis(normal);
	vec3 result = newBasis.x * direction.x + newBasis.y * direction.y + newBasis.z * direction.z;

	return normalize(result);
}

vec3 RandomGenerator::randomHemisphereDirectionCosWeighted(vec3 normal)
{
	//Determine random direction when normal is on z axis
	vec3 direction = RandomGenerator::randomUnitHemisphereDirectionCosWeighted();

	//Change Vector Basis
	basis newBasis = changeVector3DBasis(normal);
	vec3 result = newBasis.x * direction.x + newBasis.y * direction.y + newBasis.z * direction.z;
	
	return normalize(result);
}

vec3 RandomGenerator::randomSphereDirection()
{
	//Random value
	float r1 = RandomGenerator::getRandomNormalizedFloat();
	float r2 = RandomGenerator::getRandomNormalizedFloat();

	float phi = 2.0f * (float)M_PI * r1;
	float coef = sqrt(r2 * (1 - pow(r2, 2)));

	//Determine point on sphere
	vec3 direction = vec3(0, 0, 0);
	direction.x = 2 * cos(phi) * coef;
	direction.y = 2 * sin(phi) * coef;
	direction.z = (1 - 2 * r2);

	return normalize(direction);
}

vec3 RandomGenerator::randomRectPoint()
{
	float x = RandomGenerator::getRandomNormalizedFloat() - 0.5f;
	float y = RandomGenerator::getRandomNormalizedFloat() - 0.5f;

	return vec3(x, y, 0);
}