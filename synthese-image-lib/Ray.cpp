#include "pch.h"
#include "Ray.h"
#include <stdexcept>


using namespace glm;

Ray::Ray(vec3 origin, vec3 direction)
{
	if (direction == vec3(0, 0, 0)) {
		throw std::invalid_argument("Direction of Ray can't be the zero vector.");
	}

	this->origin = origin;
	this->direction = normalize(direction);
}

Ray Ray::shiftedRay() {
	return Ray(this->origin + this->direction*0.03f, this->direction);
}
