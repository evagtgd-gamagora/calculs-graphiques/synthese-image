#include "pch.h"
#include "Intersection.h"
#include <algorithm>
#include <math.h>
#include "BoxStructure.h"

using namespace std;
using namespace glm;

bool Intersection::intersectScene(bool nearest, const float dMax, const Ray ray, const vector<Intersectable*>& intersectables, const Sphere** intersectingSphere, vec3* intersection, float* distance)
{
	float currentDistance;
	vec3 currentIntersection;
	*distance = FLT_MAX;

	vector<Sphere*> spheres;
	vector<BoxStructure*> boxStructures;

	for (Intersectable* pi : intersectables)
	{
		if (Sphere * s = dynamic_cast<Sphere*>(pi))
		{
			spheres.push_back(s);
		}
		else if (BoxStructure * bs = dynamic_cast<BoxStructure*>(pi))
		{
			boxStructures.push_back(bs);
		}
	}

	vector<BoxStructure*> currentBS;
	BoxStructure* current;

	for (auto& bs : boxStructures)
	{
		currentBS.push_back(bs);

		while (!currentBS.empty())
		{
			current = currentBS.back();
			currentBS.pop_back();

			if (current->intersect(ray, &currentIntersection, &currentDistance))
			{
				if (current->sphere != nullptr)
				{
					spheres.push_back(current->sphere);
				}
				else
				{
					currentBS.push_back(current->subA);
					currentBS.push_back(current->subB);
				}
			}
		}
	}

	for (auto& sphere : spheres)
	{
		if (sphere->intersect(ray, &currentIntersection, &currentDistance))
		{
			if (currentDistance < *distance)
			{
				*intersectingSphere = sphere;
				*distance = currentDistance;
				*intersection = currentIntersection;

				if (!nearest && (*distance < dMax))
					return true;
			}
		}
	}

	

	if ((*intersectingSphere == nullptr) || (*distance > dMax))
		return false;
	else
		return true;
}
