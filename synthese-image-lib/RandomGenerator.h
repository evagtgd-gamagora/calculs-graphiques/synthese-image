#pragma once
#define _USE_MATH_DEFINES
#include <random>
#include <glm/glm.hpp>

class RandomGenerator
{
public:
	struct basis
	{
		glm::vec3 x;
		glm::vec3 y;
		glm::vec3 z;
	};
	static float getRandomNormalizedFloat();
	static glm::vec3 randomUnitHemisphereDirectionSolidAngle();
	static glm::vec3 randomUnitHemisphereDirectionCosWeighted();
	static basis changeVector3DBasis(glm::vec3 normal);
	static glm::vec3 randomHemisphereDirectionSolidAngle(glm::vec3 normal);
	static glm::vec3 randomHemisphereDirectionCosWeighted(glm::vec3 normal);
	static glm::vec3 randomSphereDirection();
	static glm::vec3 randomRectPoint();
private:
	static std::uniform_real_distribution<float> uniformRand;
	static std::mt19937 randomEngine;
};

