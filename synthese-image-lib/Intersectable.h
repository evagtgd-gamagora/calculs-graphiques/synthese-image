#pragma once
#include <glm/glm.hpp>
#include "Ray.h"

class Intersectable
{
public:
	virtual bool intersect(Ray ray, glm::vec3* intersection, float* distance) const = 0;
};

